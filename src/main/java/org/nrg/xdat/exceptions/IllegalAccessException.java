/*
 * core: org.nrg.xdat.exceptions.IllegalAccessException
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */


package org.nrg.xdat.exceptions;

/**
 * @author Tim
 *
 */
@SuppressWarnings("serial")
public class IllegalAccessException extends Exception {
	public IllegalAccessException(String message)
	{
		super(message);
	}
}

