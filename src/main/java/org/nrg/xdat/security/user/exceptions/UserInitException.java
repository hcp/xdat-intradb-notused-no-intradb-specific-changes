/*
 * core: org.nrg.xdat.security.user.exceptions.UserInitException
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xdat.security.user.exceptions;

public class UserInitException extends Exception {

    public UserInitException(String message) {
        super(message);
    }
}