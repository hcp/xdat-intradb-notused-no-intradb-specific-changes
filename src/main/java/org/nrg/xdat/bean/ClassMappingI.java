/*
 * core: org.nrg.xdat.bean.ClassMappingI
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xdat.bean;

import java.util.Map;

public interface ClassMappingI {

	public abstract Map<String, String> getElements();

}