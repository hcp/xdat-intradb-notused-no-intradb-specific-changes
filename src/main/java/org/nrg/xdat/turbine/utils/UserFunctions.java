/*
 * core: org.nrg.xdat.turbine.utils.UserFunctions
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xdat.turbine.utils;

import org.nrg.xft.security.UserI;

public class UserFunctions {
	final UserI user;
	public UserFunctions(final UserI u){
		user=u;
	}
}
