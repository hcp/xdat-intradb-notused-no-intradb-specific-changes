/*
 * core: org.nrg.xdat.daos.XdatUserAuthDAO
 * XNAT http://www.xnat.org
 * Copyright (c) 2016, Washington University School of Medicine and Howard Hughes Medical Institute
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 */

package org.nrg.xdat.daos;

import org.nrg.framework.orm.hibernate.AbstractHibernateDAO;
import org.nrg.xdat.entities.XdatUserAuth;
import org.springframework.stereotype.Repository;

@Repository
public class XdatUserAuthDAO extends AbstractHibernateDAO<XdatUserAuth> {

}
